﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Class1
    {
        /// <summary>
        /// Esta funcion me permite invertir el orden de un numero de 3 cifras realizando operaciones basicas y modulo de un numero
        /// </summary>
        /// <param name="x"> El parametro x es el numero que se le solicita al usurio y con el  que vamos a trabajar </param>
        /// <returns> Res es numero resultante ya invertido dentro de esta funcion </returns>
        public static int inverteNum(int x)
        {
            int dec, uni, cen, res = 0; ;
            cen = x / 100;
            x = x % 100;
            dec = x / 10;
            uni = x % 10;
            res = (uni * 100) + (dec * 10) + cen;
            return res;
        }
        public static float vamosPorAnvorguesas(byte canB, byte cantPapas, byte cantRefresco)
        {
            float totalCuenta = 0;
            const float precioB = 0.8f;
            const float precioH = 2;
            const float precioP = 1.2f;
            totalCuenta = (canB * precioH) + (precioP * cantPapas) + (cantRefresco * precioB);
            return totalCuenta;


        }


    }
}