﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Este Programa Tienen Ejemplos De Como Usar [if], [else if], \n" +
                 "[for], [switch/case], En Diferentes Situaciones! \n" );
            int opcionE = 0;
            Console.WriteLine("Estimado Usuario Para Poder Testear Este Software Debe Ingresar Un Numero \n" +
                "Comprendido Entre El 1 y El 5, Gracias \n" +
                "\n" + 
                "opcion 1 inversor de numeros \n");
            opcionE = int.Parse(Console.ReadLine());
            switch (opcionE)
            {
                case 1:
                    Console.WriteLine("****************- Primera Seccion -********************************");
                    Console.WriteLine("Este Programa Invierte La Posicion De Tres Numeros");
                    int valor, invert;
                    Console.WriteLine("Ingrese Numero De Tres Cifras : \n");
                    valor = int.Parse(Console.ReadLine());
                    invert = Class1.inverteNum(valor);
                    Console.WriteLine("El Numero Invertido Es: \n" + invert);
                    break;
                case 2:
                    Console.WriteLine("****************- Segunda Seccion -********************************");
                    byte canB, canH, canP;
                    float totalCuenta;
                    Console.WriteLine("Este Programa Emula Ser La Caja De Un Establecimiento De Anvurguesas :P");
                    Console.Write("Cuantas anvorguesas te atascaste : \n");
                    canH = byte.Parse(Console.ReadLine());
                    Console.Write("Cuantas papitas te empacaste : \n");
                    canP = byte.Parse(Console.ReadLine());
                    Console.Write("Cuantas litros de soda te sampaste : \n");
                    canB = byte.Parse(Console.ReadLine());
                    totalCuenta = Class1.vamosPorAnvorguesas(canH, canP, canB);
                    Console.WriteLine("Su cerdiada les va costar: \n" + totalCuenta);
                    break;

                default:
                    Console.WriteLine("Esta Opcion No Existe Compitas :P \n");
                    break;

            }
            Console.WriteLine("Pulse una Tecla Para Salir: \n");
            Console.ReadKey();



        }


    }
}


